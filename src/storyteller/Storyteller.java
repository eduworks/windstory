package storyteller;

import character.Syujinkou;
import enemy.Army;
import enemy.Hunter;
import enemy.Shark;

public class Storyteller {

	//ストーリー：１
	public void story01() {
		System.out.println("タイトル ： ー 風の生まれる場所 ー\n");

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String text ="";
		text+="それは、ごうごうと風が吹きすさぶ惑星だった・・・。\n";
		text+="俺はいつからここにいるのだろう・・・\n";
		text+="いつから戦いを俺は止めてしまったのか？\n\n";

		text+="風の吹き荒ぶ中、俺は鉄クズの混ざった砂漠を彷徨っていた・・・。\n";
		System.out.println(text);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String text2="";
		text2+="ガガガガガ・・・\n";
		text2+="後方を被弾した宇宙船が横切った\n";
		text2+="俺はギシギシ鳴る体を動かした・・・\n";
		System.out.println(text2);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	//選択
	System.out.println("ー 選択型 STORY GAME を始めます ー\n");

	System.out.println("半角数字を選び入力して下さい。\n １：後ろを振り向いた　２：上を見上げた");

	int choose =new java.util.Scanner(System.in).nextInt();

	if(choose==1){
			System.out.println("《 ドォーン！ 》\n近くに破片が落ちた、その先に永遠と続く足跡に気が付いた・・・");
			System.out.println("俺はどこから歩いてきたんだ・・・記憶がない・・・\n");

	}else if(choose==2){
		System.out.println("男は降ってきた鉄クズに押しつぶされた\n STORY GAMEOVER");
		System.exit(0);
	}


		System.out.println("ふと、遠くに人影のようなものが何かを言っている・・・");
		System.out.println("ー ごうごうと鳴る風に声は聞こえない ー");
		System.out.println("そうだ・・・俺はこの人影に導かれてここまで来た\n");
	}

	//選択１ー分岐ー　バトル・ストーリー
		public void story02(){

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			String text3="";
			text3+="《 ザザザー 》\n";
			text3+="砂の中からフードを被ったハンターが現れた！！\n";
			text3+="何者なんだ？\n思い出せない・・・\n";
			text3+="バックシステムが検索し、視界スクリーンに表示した。\n";
			text3+="元戦士・・\n";
			text3+="プログラムにバグを起こし他の戦士の部品を集める者たち・・・\n";
			text3+="視界スクリーンに武器の選択が表示される。";

			System.out.println(text3);

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

		}

//選択２/戦闘シーン
public void choose2(){

	Syujinkou s = new Syujinkou();
	Hunter h = new Hunter();

for(;;){
	if(s.getHp()>0){
		if(h.getHp()>10){
			System.out.println("\n選択　１：ハンドガン　２：電導高速体術　３：陽子レーザーガン");

			int choose2 =new java.util.Scanner(System.in).nextInt();

			switch(choose2){
				case 1:
					System.out.println("ハンターは素早く避けた！ ");
					System.out.println("弾道がハンターの腕をカスル！");
					s.attack_h(h);
					System.out.println("ハンターの素早い反撃！ボディパーツを奪われる");
					h.attacks_h(s);
					break;
				case 2:
					System.out.println("ハンターは素早く砂に潜った！\n砂中からハンターの反撃！ボディパーツを奪われる");
					h.attacks_h(s);
					break;
				case 3:
					System.out.println("距離を取りレーザーガンで狙いを定める！");
					s.laserAttack(h);
					System.out.println("ハンターに命中！残り敵HP"+h.getHp());
					break;
			}
			}else{
			System.out.println("\nハンターは逃げ出した。\n人影はいなくなっていた・・・\n");
			break;
		}
	}else{
				System.out.println("HPが無くなった！システム停止");
				System.out.println("STROY GAMEOVER");
				System.exit(0);
				}
			}
}

		public void story03(){

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			System.out.println("《 ゴォォォーーー 》\n俺は磁気嵐の中に遭遇してしまっていた。");
			System.out.println("磁気がひどく方向マップも効かない\n");

			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			System.out.println("うっすらと人影が現れた・・・\n来るなという素振りをした・・・\n");

			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			String text="";
			text+="人影が消え、敵の軍隊が現れた！\n";
			text+="しまった！磁気でセンサーが反応がなかった。\n";
			text+="陽子レーザーガンの雨が来た！！\n";
			text+="岩影に飛び込みながら、ボディの経験が反応した！\n";

			System.out.println(text);

		}

		//選択３
		public void choose3(){
			Syujinkou s =new Syujinkou();
			Army ay=new Army();

for(;;){
	if(s.getHp()>0){
		if(ay.getHp()>10){
			System.out.println("選択１：陽子レーザーガン　２：電子手りゅう弾　３：電導高速体術\n");

			int choose3 =new java.util.Scanner(System.in).nextInt();


			switch(choose3){
			case 1:
				System.out.println("レーザーガンで応戦、ボディに数弾喰らう！");
				System.out.println("敵を数体倒した");
				ay.attack_a(s);
				break;
			case 2:
				System.out.println("広範囲で応戦、ボディに数弾喰らう！");
				System.out.println("敵を広範囲で倒した！");
				ay.attack_a(s);
				break;

			case 3:
				String text5= "";
				text5+="ハンドガンに手を掛け、電導高速体術モードに切り替えた！\n";
				text5+="第二波のレーザーの雨が来る隙を尽き、\n";
				text5+="音速で突っ込み音速で生まれる風圧で数十体が吹き飛ぶ！\n";
				text5+="電導高速体術で戦いながら編隊を突き破る\n";
				text5+="編隊が散り出した！\n";
				System.out.println(text5);
				s.MartialArt(ay);
				break;
				}
			}else{
				break;
			}
		}else{
				System.out.println("HPが無くなった！システム停止");
				System.out.println("STROY GAMEOVER");
				System.exit(0);
			}
		}
	}

		public void story04(){


			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			System.out.println("真下の砂が盛り上がった！\n巨大な潜行飛行艇が現れた\n");
			System.out.println("選択１：船にしがみ付く　２：船から飛び降りる\n");

			int choose =new java.util.Scanner(System.in).nextInt();

			//Ship選択１
			if(choose==1){

			String text="";
			text+="潜行飛行艇は空に急上昇した。\n";
			text+="表面は砂の中を移動するため抵抗がない。\n";
			text+="滑りながらハッチを掴んだ。\n";
			text+="船が俺の存在を知り警報を発していた・・・\n";

			System.out.println(text);

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			}else if(choose==2){
				System.out.println("待ち構えていたアーミーのレーザーでハチの巣となる");
				System.out.println("STORY GAMEOVER");
				System.exit(0);
			}

			System.out.println("船の中、\n視界モニターに敵の反応が映し出された。");
			System.out.println("凄まじい数の敵と２０秒後に遭遇する\n逃げ道は無い\n");

			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			System.out.println("ー生き残りなさいー \n微かな声が過った！\n");

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			String text2="";
			text2+="視界モニターに\n";
			text2+="オート狂戦士モードの推薦が表示された。\n\n";
			text2+="オートバトルモードはエネルギーを凄まじく使い\n";
			text2+="ボディが破壊されるまで戦い続ける。\n";
			text2+="一度使ったら最後だ！\n";
			System.out.println(text2);

			System.out.println("選択１：オート狂戦士モードを使う　２：通常に戦う\n");

			int choose2 =new java.util.Scanner(System.in).nextInt();

			//Ship選択２
			if(choose2==1){
			String text3="";
			text3+="「 当システムはオート バーサーカー モードに移行します。 」\n";
			text3+="声と共に、";
			text3+="もう一つのバックシステムが解放され、\n";
			text3+="俺はボディと意識から切り離された。\n";
			text3+="オート狂戦士モードは今まで蓄積した、\n";
			text3+="全ての戦いの記憶と経験を全開で出し戦い続ける。\n";
			System.out.println(text3);

			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			String text4="";
			text4+="ノイズ交じりの視界の中、\n";
			text4+="無慈悲なまでの戦いが繰り広げられた。\n\n";

			text4+="凄まじいまでの鉄片と火花が散らばる\n";
			text4+="敵のものか、俺のものなのか分からない\n";
			System.out.println(text4);

			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			String text5="";
			text5+="着実に一人ずつ数コンマの差で隙を突き、破壊を繰り返し続けた。\n";
			text5+="動けなくなった者も、逃げ出す者も着実に破壊し続けた。\n";
			text5+="俺はこんな戦い方をしていたのか・・・\n\n";
			text5+="それは忘れてしまった、過去を再現していた。\n";
			text5+="ボディが壊れながらもいつ終わるとも知れない戦闘の中\n\n";

			text5+="人影が何度も現れた。\n";
			text5+="彼女は何度も言った\n";
			text5+=" ー 生き残れと ー \n";
			System.out.println(text5);

			try {
				Thread.sleep(6000);
			} catch (InterruptedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			String text6="";
			text6+="船は風が吹き続ける中を航行していた。\n";
			text6+="ボロボロのボディを引きずりながら、とうとう無人の中枢コクピットまでやってきた。\n\n";
			text6+="「 何者だ！」 中枢システムは言った。\n";
			text6+="バーサーカーモードは有無を言わず、中枢システムのメモリーを引きずり出した。\n";
			text6+="青白い火花と幾つものケーブルを散らしながら、\n";
			text6+="中枢プログラムは崩壊した。\n";
			System.out.println(text6);

			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			String text7="";
			text7+="バーサーカーモードのもう一つの目的。\n";
			text7+="それは、敵の情報を奪うこと。\n";
			text7+="バーサーカーモードは自らのメモリー領域に敵のメモリーのダウンロードを始めた。\n";
			text7+="凄まじい量にシステムは停止した。\n";
			text7+="制御を失った船と共に暴風雨の中を降下していった・・・\n";
			System.out.println(text7);

			try {
				Thread.sleep(6000);
			} catch (InterruptedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			String text8="";
			text8+="「 目を覚まして！！ 」\n";
			text8+="風の音の中、声が聞こえた！\n";
			text8+="強風の音と共に意識を取り戻した。\n\n";
			text8+="コクピットのガラスが割れ、風が吹き込んでいた\n\n";

			text8+="選択 １：這いずりながら窓から飛び降りる　２：そのままコクピットにしがみ付く\n";

			System.out.println(text8);

			}else if(choose2==2){
				System.out.println("一瞬のスキを突かれボディが破壊された！！\nシステム停止\n");
				System.out.println("STORY GAMEOVER");
				System.exit(0);
			}

			//Ship選択３
			int choose3 =new java.util.Scanner(System.in).nextInt();

			if(choose3==1){

				System.out.println("ーエンターンキーを入力して下さい ー\n");
				System.out.println("ー以降-fin-が表示されるまで表示が、\n止まった ままの場合ENTER KEY入力となりますー");
			String enter =new java.util.Scanner(System.in).nextLine();

			}else if(choose3==2){
				System.out.println("船と共に砂山に突っ込み跡形もなく大破した！");
				System.out.println("STORY GAMEOVER");
				System.exit(0);
			}


			System.out.println("気分はどう？\n彼女の声がはっきりと聞こえた。");

			//ー ENTER KEY ー
			String enter2 =new java.util.Scanner(System.in).nextLine();

			String text9="";
			text9+="メモリーがパンクしそうだ。\n";
			text9+="俺はゆっくりと目を開いた。\n";
			text9+="そこには透き通った、ホログラムの女が見下ろしていた。";
			System.out.println(text9);

			//ー ENTER KEY ー
			String enter3 =new java.util.Scanner(System.in).nextLine();


			System.out.println("君は？\n");

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			System.out.println("私はこの惑星の管理システムを司っていた者\n私の名は AI");

			//ー ENTER KEY ー
			String enter4 =new java.util.Scanner(System.in).nextLine();

			System.out.println("風が止んでいる・・・\nここは？\n");

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			System.out.println("風の生まれる場所");

			//ー ENTER KEY ー
			String enter5 =new java.util.Scanner(System.in).nextLine();


			System.out.println("惑星の大気の風は全てここから始まるの。");
			System.out.println("そしてこのエリアは私の管理領域");

			//ー ENTER KEY ー
			String enter6 =new java.util.Scanner(System.in).nextLine();


			System.out.println("遠い昔、あなたたちはこの惑星から大気を無くしてしまった。");

			//ー ENTER KEY ー
			String enter7 =new java.util.Scanner(System.in).nextLine();


			System.out.println("君がその大気を作っているのか？");

			//ー ENTER KEY ー
			String enter8 =new java.util.Scanner(System.in).nextLine();


			System.out.println("いいえ、正確にはあなた達よ。");
			System.out.println("あなたたちの壊れた部品を使って、この風を生み出すモノを\n作り出した。");

			//ー ENTER KEY ー
			String enter9 =new java.util.Scanner(System.in).nextLine();

			System.out.println("俺もその部品となるのか？");

			//ー ENTER KEY ー
			String enter10 =new java.util.Scanner(System.in).nextLine();


			System.out.println("彼女は悲しい表情をした。");
			System.out.println("今までのロボットで見たことのない表情だった。");

			//ー ENTER KEY ー
			String enter11 =new java.util.Scanner(System.in).nextLine();

			System.out.println("そんな表情をする者は見たことが無い・・・\nどんな意味があるんだ？");

			//ー ENTER KEY ー
			String enter12 =new java.util.Scanner(System.in).nextLine();

			System.out.println("私の創造主は言ったわ。\nそれは「 Love 」だと・・・");

			//ー ENTER KEY ー
			String enter13 =new java.util.Scanner(System.in).nextLine();

			System.out.println("そして、あなたもそれを知っているの・・・。");

			//ー ENTER KEY ー
			String enter14 =new java.util.Scanner(System.in).nextLine();

			System.out.println("彼女は俺の額に透き通ったホログラムの手をかざした。");

			//ー ENTER KEY ー
			String enter15 =new java.util.Scanner(System.in).nextLine();

			System.out.println("《 ガキン、ガカァァン! 》\n懐かしい金属が当たり合う音がした。");
			System.out.println("もう一人のロボットが戦いのパートナーをしていた。");

			//ー ENTER KEY ー
			String enter16 =new java.util.Scanner(System.in).nextLine();

			String text10="";
			text10+="生まれた時から俺たちはずっと一緒だった。\n";
			text10+="時は流れお互いに戦士へとなり、それぞれの戦場へと散っていった。";
			System.out.println(text10);

			//ー ENTER KEY ー
			String enter17 =new java.util.Scanner(System.in).nextLine();

			System.out.println("凄まじい激戦の中、見慣れた腕を掴んでいた。\n見慣れた胴体が横たわっていた・・・");

			//ー ENTER KEY ー
			String enter18 =new java.util.Scanner(System.in).nextLine();

			System.out.println("何故か震える手で、製造ナンバーを調べた。\n");

			//ー ENTER KEY ー
			String enter19 =new java.util.Scanner(System.in).nextLine();

			System.out.println("俺は叫んでいた・・・");

			//ー ENTER KEY ー
			String enter20 =new java.util.Scanner(System.in).nextLine();


			System.out.println("彼女の声が聞こえた\nそれは「 Love 」だと・・・");

			//ー ENTER KEY ー
			String enter21 =new java.util.Scanner(System.in).nextLine();


			String text11="";
			text11+="俺は目を覚ました。\n";
			text11+="俺は壊してしまったんだ。\n";
			text11+="彼を。\n";
			text11+="彼は敵兵となっていた\n";
			text11+="耐えられず、システムを停止しメモリーが飛んだんだ・・・";

			System.out.println(text11);

			//ー ENTER KEY ー
			String enter22 =new java.util.Scanner(System.in).nextLine();

			System.out.println("彼女は言った。\nそれは「 Love 」だと・・・");

			//ー ENTER KEY ー
			String enter23 =new java.util.Scanner(System.in).nextLine();

			String text12="";
			text12+="彼女は悲しい顔をしながら\n";
			text12+="俺が再び停止するまで見つめていた。";
			System.out.println(text12);


			//ー ENTER KEY ー
			String enter24 =new java.util.Scanner(System.in).nextLine();

			System.out.println("なぜ、ずっとここでみてるんだ？\n俺は薄れる意識で尋ねた。");

			//ー ENTER KEY ー
			String enter25 =new java.util.Scanner(System.in).nextLine();

			System.out.println("彼女は言った。\nそれは「 Love 」だと・・・");

			//ー ENTER KEY ー
			String enter26 =new java.util.Scanner(System.in).nextLine();

			String text13="";
			text13+="彼女は俺を部品として使わなかった\n";
			text13+="その代わり土に埋まり、十字の棒が刺さっていた・・・\n";
			text13+="風によって生れた緑色の何かに覆わながら、、、";
			System.out.println(text13);

			//ー ENTER KEY ー
			String enter27 =new java.util.Scanner(System.in).nextLine();


			System.out.println("彼女はこう言うだろう\nそれは「 Love 」だと・・・");

			//ー ENTER KEY ー
			String enter28 =new java.util.Scanner(System.in).nextLine();

			System.out.println("俺は透き通ったボディで、それを見つめていた。");

			//ー ENTER KEY ー
			String enter29 =new java.util.Scanner(System.in).nextLine();


			System.out.println("彼女は寄り添い、こう言た・・・");

			//ー ENTER KEY ー
			String enter30 =new java.util.Scanner(System.in).nextLine();


			System.out.println("それは「 Love 」だと・・・");

			//ー ENTER KEY ー
			String enter31 =new java.util.Scanner(System.in).nextLine();

			System.out.println("「 ー 風の生まれる場所 ー 」");
			System.out.println("-fin-");
		}


		//選択２ー分岐ー　ウィンドウ・ロード・ストーリー
		public void story05(){
		System.out.println("俺は、遠くに霞む人影に向かって歩き出した。\n");

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		String text="";
		text="幻覚なのだろうか？\n";
		text+="俺は幻覚に惑わされ彷徨っているだけなのだろうか？\n";
		text+="俺は歩き続けた。不安を感じながらも・・・\n";
		text+="霞む砂嵐の中、再びその人影が現れ何かを言っていた。\n";
		text+="メモリーマップはデータにないエリアを指していた・・・\n";
		text+="俺は未開の地を今歩いている\n";
		text+="幾つもの風車が乱立しているのが見える。\n";
		text+="風の流れがきつくなってきている。\n";
		System.out.println(text);

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("ザザザザザ！\n");

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		String text2="";
		text2+="砂嵐の中\n";
		text2+="いつのまにか、きめ細かな砂の中を移動する背びれの群れに囲まれていた\n";
		text2+="砂漠ザメ！仲間内ではそう呼んでいた。\n";
		text2+="金属ですら、エサとして食い荒らす有機体だ！\n";
		System.out.println(text2);

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("遠くで砂の山が膨れ上がった\nまだ他に何かいるようだ・・・\n");

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("砂漠ザメが襲ってきた！\n");

		//選択
		System.out.println("選択：\n １：右に避ける　２：左に避ける");

		int choose =new java.util.Scanner(System.in).nextInt();
		if(choose==1){
			System.out.println("サメの牙をわずかに避けた");
		}else if(choose==2){
			System.out.println("飛び付かれメイン駆動システムを喰われる！\nシステム停止！");
			System.out.println("STORY GAMEOVER");
			System.exit(0);
		}
		}
		//選択４
		public void choose4(){

		Syujinkou s =new Syujinkou();
		Shark sh=new Shark();

	for(;;){
		if(s.getHp()>0){
			if(sh.getHp()>10){
		System.out.println("選択：\n １：陽子レーザーガンを使う　２：　電導高速体術　３：ハンドガンを使う\n");
		int choose =new java.util.Scanner(System.in).nextInt();

		switch(choose){

		case 1:
			System.out.println("レーザーがサメを掠る！サメの動きは素早い！！");
			s.attacklaser_Sh(sh);
			sh.attack(s);
			break;

		case 2:
			System.out.println("飛びかかって来たサメの懐に入り投げ飛ばした！！");
			s.attackShark(sh);
			sh.attack(s);
			break;

		case 3:
			System.out.println("間合いを取り、数体を撃つ！！");
			s.attackGunShark(sh);
			sh.attack(s);
			break;
			}
		}else{
			break;
			}

		}else{
			System.out.println("HPがなくなった！！\nシステム停止\n");
			System.out.println("STORY GAMEOVER");
			System.exit(0);
			}
		}
	}
		public void story06(){

		System.out.println("戦闘の中、再び砂が盛り上がった\n真下から、巨大な口が開いた！\n");

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("ー ENTER KEY ー 入力で読み進めます ー\n");
		System.out.println("《 以降-fin-の文字が出るまで、表示が止まった ままの場合\n ー ENTER KEY ー入力で話が展開します。 》");

		//ー ENTER KEY ー
		String enter =new java.util.Scanner(System.in).nextLine();


		System.out.println("気分はどう？\nそこには武器を持たない透明な機体が見つめていた。");

		//ー ENTER KEY ー
		String enter2 =new java.util.Scanner(System.in).nextLine();

		System.out.println("ここは？\n");

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String text="";
		text+="砂クジラの中よ\n";
		text+="私たち食べられちゃった。\n";
		text+="砂漠ザメ達もね。\n";
		System.out.println(text);

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		String text2="";
		text2+="砂漠ザメはバタバタとして動けなかった。\n";
		text2+="砂以外の場所では動けないらしい・・・";
		System.out.println(text2);

		//ー ENTER KEY ー
		String enter3 =new java.util.Scanner(System.in).nextLine();


		System.out.println("時々現れた人影は君だったのか・・・？\n");


		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("そうよ。\n");

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		String text3="";
		text3+="それより、これどう？\n";
		text3+="彼女は透き通った機体を軽くターンした。\n";
		System.out.println(text3);

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("瞬間に色とりどりの、ひらひらの服に姿を変えた。\n");


		}

		//選択５
		public void choose5(){
			int love=300;

		for(;;){
		if(love>0 && love<1000){

		System.out.println("選択\n１：無視する　２：取り合えず褒める　３：最高に褒める\n");

		int choose =new java.util.Scanner(System.in).nextInt();

		switch(choose){

		case 1:
			System.out.println("不機嫌になった。");
			love-=200;
			System.out.println("好感度が２００ポイント下がった。"+"残り"+love+"ポイント\n");
			break;

		case 2:
			System.out.println("もう・・\nこれはどう？\n彼女は更に色とりどりの煌びやかな服に姿を変えた。\n");
			love+=200;
			System.out.println("好感度が２００ポイント上がった。"+"残り"+love+"ポイント\n");
			break;

		case 3:
		love+=400;
		System.out.println("好感度が４００ポイント上がった。"+"残り"+love+"ポイント");
		System.out.println("便利だな。\nいいでしょ。\n再び彼女は服を変えた。\n");
		break;
		}
		}else if(love>=1000){
			System.out.println("君はなぜ武器を持たないんだ？\nそれに輝いて見える。\n");

			break;
		}else{
			System.out.println("AIはぶち切れた！！\n");
			System.out.println("STORY GAMEOVER");
			System.exit(0);
			}
			}
		}
		public void story07(){

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			String text="";
			text+="なによう...\n";
			text+="照れているようだ・・・。\n";
			text+="私に名は 『 AI 』\n";
			text+="ホログラムよ。\n";
			text+="宜しく。\n";
			System.out.println(text);

			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			String text2="";
			text2+="戦場にはない笑顔をした。\n";
			text2+="些細な会話の中\n";
			text2+="ザザッ・・・\n";
			text2+="会話やホログラムがエラーで乱れる\n";
			text2+="時々彼女のプログラムは不安定な様に見える・・・";
			System.out.println(text2);

		//ー ENTER KEY ー
		String enter =new java.util.Scanner(System.in).nextLine();

		System.out.println("ようっ。二人さん。\n来客者は久々だなぁ");
		System.out.println("半分溶けた機体が座っていた。\n");

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("君は？\n");

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		String text3="";
		text3+="おまえは東側の機体か？\n";
		text3+="まあ今のおれにとっちゃどっちでもいい\n";
		System.out.println(text3);

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("どういう意味なんだ？\n君は敵側と味方側の反応もある・・・");


		//ー ENTER KEY ー
		String enter2 =new java.util.Scanner(System.in).nextLine();

		String text4="";
		text4+="困惑気味だな・・・\n";
		text4+="俺は敵側に捕虜となり、敵側で戦うよう、プログラムを書き換え作り直られた・・・\n";
		text4+="ある時爆風の衝撃でバクアッププログラムを思い出した。\n";
		text4+="だが、味方側に付くつもりがどっちからも追われる事態となった。\n";
		System.out.println(text4);

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		String text5="";
		text5+="どっち側にも付けない\n";
		text5+="俺は戦えなくなった。\n";
		text5+="惨めなものさ\n";
		text5+="そして今は胃袋の中だ。";
		System.out.println(text5);

		//ー ENTER KEY ー
		String enter3 =new java.util.Scanner(System.in).nextLine();

		String text6="";
		text6+="そこのお嬢さん。\n";
		text6+="透明な機体なんて珍しいな・・・\n";
		text6+="彼女は今までのロボットでは見たことのない表情で彼を見ていた・・・\n";
		System.out.println(text6);

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("可哀そう・・・\n彼女はつぶやいた");

		//ー ENTER KEY ー
		String enter4 =new java.util.Scanner(System.in).nextLine();


		System.out.println("なぜ、そんな表情が出来る？？\nどんな意味があるんだ？\n");

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("私の創造主はこう言っていた・・・\nそれは「 Love (); 」だと・・・\n");

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("そして、あなたもそれを知っているの・・・。\n彼女は俺をみつめた。");

		//ー ENTER KEY ー
		String enter5 =new java.util.Scanner(System.in).nextLine();

		String text7="";
		text7+="《 ザ～～～～ 》\n";
		text7+="遠くから液体のようなものが降ってきた・・・\n";
		text7+="もうすぐ、お食事タイムが来る！\n";
		text7+="溶かされるぜぇ。\n";
		System.out.println(text7);

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		String text8="";
		text8+="最後にお前たちと話せて良かった！\n";
		text8+="お前にはまだ未来がある\n";
		text8+="さあはじめるぜっ！\n\n";
		text8+="自爆コマンドを入れようとした！\n";
		System.out.println(text8);

		System.out.println("選択：１：止めようとする。２：自爆コマンド入力を手伝う");

		//選択
		int choose =new java.util.Scanner(System.in).nextInt();

		if(choose==1){
		String text9="";
		text9+="ダメだ！\n\n";
		text9+="なぜ止める？\n俺たちは戦える機体を優先させるよう、プログラムさてれいるはずだ。\n";
		System.out.println(text9);



		}else{
			System.out.println("一緒に自爆した！！\n AIは呆れ笑顔となった！！");
			System.out.println("STORY GAMEOVER");
			System.exit(0);
		}



		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		System.out.println("さあ、行け！\n");

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("自爆シークエンスを発動させた。\n");

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("爆風の中、俺は過去を思い出していた・・・");

		String enter6 =new java.util.Scanner(System.in).nextLine();

		String text10="";
		text10+="ガキン、ガカァァン!\n";
		text10+="懐かしい金属が当たり合う音が聴こえた。\n";
		text10+="もう一人のロボットが戦いのパートナーをしていた。\n";
		text10+="生まれた時から俺たちはずっと一緒だった。\n";
		System.out.println(text10);

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("時は流れお互いに戦士へとなり、それぞれの戦場へと散っていった。\n");

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		String text11="";
		text11+="凄まじい激戦の中、見慣れた腕を掴んでいた。\n";
		text11+="見慣れた胴体が横たわっていた。";
		System.out.println(text11);

		String enter7 =new java.util.Scanner(System.in).nextLine();


		System.out.println("何故か震える手で製造ナンバーを調べた。\n");

		//ー ENTER KEY ー
		String enter8 =new java.util.Scanner(System.in).nextLine();

		System.out.println("俺は叫んでいた・・・");

		//ー ENTER KEY ー
		String enter9 =new java.util.Scanner(System.in).nextLine();


		System.out.println("叫びの声で目を覚ました・・・\n俺は壊してしまったんだ、、、");

		//ー ENTER KEY ー
		String enter10 =new java.util.Scanner(System.in).nextLine();


		String text12="";
		text12+="俺はよろよろと立ち風車が乱立する砂漠を歩いていた\n";
		text12+="彼女の姿は消えていた・・・";
		System.out.println(text12);

		//ー ENTER KEY ー
		String enter11 =new java.util.Scanner(System.in).nextLine();

		String text13="";
		text13+="エネルギーが切れかかっている\n";
		text13+="風車で補給できるだろうか？\n\n";
		System.out.println(text13);

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("選択：\n １：風車に入る　２：そのまま根性で進む\n");


		//選択２
		int choose2 =new java.util.Scanner(System.in).nextInt();

		if(choose2==1){
		String text14="";
		text14+="フォフォッフォッ\n";
		text14+="懐かしいのぉ。\n";
		text14+="昔、お前さんのように戦場を駆け回っておった・・・\n";
		text14+="暗闇で老人の声がし、\n";
		text14+="風車のシステムに明かりが灯り出した。\n";
		System.out.println(text14);

		}else if(choose2==2){
			System.out.println("砂嵐の中、古い風車が崩れてきた！！\n下敷きとなった\n");
			System.out.println("STORY GAMEOVER");
			System.exit(0);
		}

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("あなたは？\n");

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		String text15="";
		text15+="わしか？\n";
		text15+="周りはウインドメーカーと呼んでおるようじゃ。\n";
		text15+="いま、わしは一つの風を作り出すシステムを営んでおる。";
		text15+="長い時をな。";
		System.out.println(text15);

		//ー ENTER KEY ー
		String enter12 =new java.util.Scanner(System.in).nextLine();

		String text16="";
		text16+="じゃが、わしもそろそろ限界じゃ\n";
		text16+="いつシステムが停止してもおかしくない\n";
		text16+="長く回り過ぎた・・・";
		System.out.println(text16);

		//ー ENTER KEY ー
		String enter13 =new java.util.Scanner(System.in).nextLine();

		System.out.println("ん・・・？\n");

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("そのような顔をするのは二人しか見たことがない");
		System.out.println("おまえさんと、・・・\n");

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("まあ、いずれ分かることじゃ。\nそんな顔をするでない。\n");

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("見てごらん、\nあの鳥を。");


		//ー ENTER KEY ー
		String enter14 =new java.util.Scanner(System.in).nextLine();


		System.out.println("剥げ落ちた壁の隙間から、空を飛ぶ鳥が見えた。\n");

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("創設者は 鷹 と言っておったようじゃ。\n");

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		String text17="";
		text17+="あの鳥は わしらのように狩りをする。\n\n";
		text17+="そして時々 わしの作った風の上で、優雅に飛んで楽しみよる。\n";
		text17+="それで良いのじゃ。";
		System.out.println(text17);

		//ー ENTER KEY ー
		String enter15 =new java.util.Scanner(System.in).nextLine();


		System.out.println("ところで若いの、\n透明な機体と出会わなかったか？\n");

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}


		System.out.println("出会った・・・\n彼女はなんなのだ？\n");

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		String text18="";
		text18+="わしは戦闘で壊れかけた際\n";
		text18+="彼女に導かれての・・・。\n";
		text18+="回収されたのじゃ。";
		System.out.println(text18);

		//ー ENTER KEY ー
		String enter16 =new java.util.Scanner(System.in).nextLine();



		System.out.println("敵も味方も関係なく、\n"
				+ "ここにある風車は皆戦えなくなった者で出来ておる\n");

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		String text19="";
		text19+="この世界は一度滅んだのだ、\nわしらによってな。\n";
		text19+="大気を無くしてしまったのじゃ\n";
		text19+="遠い昔のことじゃ";
		System.out.println(text19);

		//ー ENTER KEY ー
		String enter17 =new java.util.Scanner(System.in).nextLine();


		System.out.println("戦争で生き残った、一人の女性科学者が");
		System.out.println("惑星管理システムだった彼女を、作り変えたのじゃ\n");

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("そして頼んだ・・・\n風を蘇えらせろと・・・");

		//ー ENTER KEY ー
		String enter18 =new java.util.Scanner(System.in).nextLine();

		String text20="";
		text20+="彼女も崩壊の兆しがある\n";
		text20+="長い間ひとりで頑張ってきた\n";
		text20+="メモリーのカウントすら消える程の時をな・・・\n";
		System.out.println(text20);

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("彼女とその後はどうなるんだ？\nそれはわしにも分からん。");

		//ー ENTER KEY ー
		String enter19 =new java.util.Scanner(System.in).nextLine();

		String text21="";
		text21+="さて、そろそろエネルギーを補給できたじゃろう？\n";
		text21+="ファースト・ウインドウはもうすぐだ。\n";
		text21+="行きなさい。\n";
		System.out.println(text21);

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("ファースト・ウインドウ ・・？");

		//ー ENTER KEY ー
		String enter20 =new java.util.Scanner(System.in).nextLine();

		System.out.println("風の始まる場所じゃ・・・\n\n俺は老戦士と別れを告げた。");

		//ー ENTER KEY ー
		String enter21 =new java.util.Scanner(System.in).nextLine();

		System.out.println("林立する風車が無くなり、緑色の何かに覆われたエリアが現れた。\n");

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("ようこそ、ー 風の生まれる場所へ  ー\n\n透き通る、彼女が現れ言った。\n");

		//ー ENTER KEY ー
		String enter22 =new java.util.Scanner(System.in).nextLine();


		System.out.println("物陰からハンターが現れた！\n");

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		System.out.println("獲物を壊し部品を奪う者たち");

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		}

		//選択６
		public void choose6(){

		Syujinkou s = new Syujinkou();
		Hunter h = new Hunter();

	for(;;){
		if(s.getHp()>0){
			if(h.getHp()>10){
				System.out.println("\n選択\n　１：ハンドガン　２：電導高速体術　３：彼女を守る");

				int choose =new java.util.Scanner(System.in).nextInt();

				switch(choose){
					case 1:
						System.out.println("ハンターは素早く避けた ");
						System.out.println("弾道がハンターの腕をカスル！");
						s.attack_h(h);
						System.out.println("ハンターの素早い反撃！\nボディパーツを奪われる");
						System.out.println("ハンター達は、怪しげな喜びのダンスを踊った！");
						h.attacks_h(s);
						break;
					case 2:
						System.out.println("ハンターは素早く砂に潜った！\nハンターの反撃、ボディパーツを奪われる");
						System.out.println("ハンター達は、怪しげな喜びのダンスを踊った！");
						h.attacks_h(s);
						break;
					case 3:
						System.out.println("ガンを彼女に向けた。\nとっさに彼女をかばった！\n");
						s.protect_ai(h);
						break;
				}
				}else{

				break;
			}
		}else{
					System.out.println("HPが無くなった！システム停止");
					System.out.println("STROY GAMEOVER");
					System.exit(0);
					}
				}
		}

		public void story08(){

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			System.out.println("ボディが 砕け散る音がした！");

		//ー ENTER KEY ー
		String enter =new java.util.Scanner(System.in).nextLine();

		System.out.println("\n木漏れ日の中、そよ風が吹いていた・・・");

		//ー ENTER KEY ー
		String enter2 =new java.util.Scanner(System.in).nextLine();

		System.out.println("彼女の膝の上で目が覚めた・・・\n");

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		String text="";
		text+="馬鹿ね。\n";
		text+="私はホログラムよ。\n";
		text+="武器は効かないわ。\n";
		text+="彼女は見下ろしながら言った。\n";
		System.out.println(text);

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String text2="";
		text2+="ホログラムとはそういうものなのか・・・\n";
		text2+="身体が軽い・・・\n";
		text2+="俺のボディは透けていた。";
		System.out.println(text2);

		//ー ENTER KEY ー
		String enter3 =new java.util.Scanner(System.in).nextLine();

		System.out.println("これは？\n\n私を助けてくれたお礼よ。\n");

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String text3="";
		text3+="あなたのメタルボディはもうないわ\n";
		text3+="彼らにあげたから・・・\n";
		text3+="もう戦う必要はないでしょ？";
		System.out.println(text3);

		//ー ENTER KEY ー
		String enter4 =new java.util.Scanner(System.in).nextLine();

		System.out.println("木漏れ日の午後、そよ風が吹いていた・・・");

		//ー ENTER KEY ー
		String enter5 =new java.util.Scanner(System.in).nextLine();


		System.out.println("緑色の草木の中、何気無い時を彼女と過ごす・・・");
		System.out.println("彼女のプログラムは日に日に安定性を取り戻していった。");

		//ー ENTER KEY ー
		String enter6 =new java.util.Scanner(System.in).nextLine();

		System.out.println("木漏れ日の朝、そよ風が吹いていた・・・");

		//ー ENTER KEY ー
		String enter7 =new java.util.Scanner(System.in).nextLine();


		System.out.println("彼女の膝の上で、\n恒星の温かさとは違うものを感じていた。");

		//ー ENTER KEY ー
		String enter8 =new java.util.Scanner(System.in).nextLine();

		System.out.println("この温かさはなんなのだろう・・・？");

		//ー ENTER KEY ー
		String enter9 =new java.util.Scanner(System.in).nextLine();


		System.out.println("彼女は優しくささやいた\nそれは「 Love 」・・・");

		//ー ENTER KEY ー
		String enter10 =new java.util.Scanner(System.in).nextLine();


		System.out.println("木漏れ日の中、そよ風が吹いていた・・・\n");

		//ー ENTER KEY ー
		String enter12 =new java.util.Scanner(System.in).nextLine();

		System.out.println("「 ー 風の生まれる場所 ー 」");
		System.out.println("- fin -");

	}
}

