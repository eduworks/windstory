package character;

import enemy.Army;
import enemy.Hunter;
import enemy.Shark;

public class Syujinkou {

	int hp=100;
	int attackGun_h=5;

	 public void syujinkou(){
		this.hp=100;
	}

	//対ハンター
	public void attack_h(Hunter h){
		h.setHp(h.getHp() - 5);
			System.out.println("敵に"+this.attackGun_h+"ポイントのダメージをあたえる！\n敵HP"+h.getHp());
	}

	public void laserAttack(Hunter h){
		h.setHp(h.getHp() - 20);
	}

	public void protect_ai(Hunter h){
		h.setHp(h.getHp() - 40);
	}

	//対アーミー
	public void MartialArt(Army ay){
		ay.setHp(ay.getHp() - 45);
	}
//shark
	//対 砂漠サメ
	public void attacklaser_Sh(Shark sh){

		sh.setHp(sh.getHp() - 10);
		System.out.println("敵、1０のダメージ"+"残りHP"+sh.getHp());
	}

	public void attackShark(Shark sh){

		sh.setHp(sh.getHp() - 20);
		System.out.println("敵、20のダメージ"+"残りHP"+sh.getHp());
	}

	public void attackGunShark(Shark sh){

		sh.setHp(sh.getHp() - 30);
		System.out.println("敵、３０のダメージ"+"残りHP"+sh.getHp());
	}



	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}
}
