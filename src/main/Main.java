package main;

import storyteller.Storyteller;

	//物語はプログラム、人工知能をテーマにしています。
	//Image_mainはイメージ画像です。
	//攻略法
	//２個選択は全て１を選択
	//３個選択は全て３を選択
	//もう一つのストーリーは始まり２回目の選択で分岐します。

public class Main {
	public static void main(String[] args){

		Storyteller st = new Storyteller();

		st.story01();

		//選択:
		//ー分岐ーバトル・ストーリー＆ウィンドウ・ロード・ストーリー
		System.out.println("数字を入力して下さい。\n １：人影とは反対方向に向く　２：人影に向かって歩く\n");
		int choose =new java.util.Scanner(System.in).nextInt();
		if(choose==1){

			System.out.println("何か気配を感じた・・・\n");

		//選択１ー分岐ー　バトル・ストーリー
		st.story02();
		st.choose2();
		st.story03();
		st.choose3();
		st.story04();

	}else if(choose==2){

		//選択２ー分岐ー　ウィンドウ・ロード・ストーリー
		st.story05();
		st.choose4();
		st.story06();
		st.choose5();
		st.story07();
		st.choose6();
		st.story08();
		}
	}
}
