package enemy;

import character.Syujinkou;

public class Shark {

	int hp=70;
	int attack_syu=20;

	public void attack(Syujinkou s){

		s.setHp(s.getHp() - 20);
		System.out.println("砂漠サメ、牙の攻撃"+this.attack_syu + "のダメージ！ \n残りHP"+s.getHp()+"\n");

	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}


}
